# mobilender-frontend

## Autor
```
Daniel Gómez

fradagoso@gmail.com
```

## Instalación de dependencias
```
npm install
```

### Correr el proyecto con hot-reloads
```
npm run serve
```

### Compilación y minificación para producción
```
npm run build
```

### Correr pruebas unitarias
```
npm run test:unit
```

### Personalización de API, tema de colores e idioma.
* Para configurar la url de la API, es necesario declarar una variable de entorno llamada `API_URL` o entrar al archivo `axios.js` dentro de la carpeta `plugins`, en la linea 4 se encuentra la variable `baseUrl` que corresponde a la url de la API a consumir.

* Para modificar los colores del proyecto, es necesario modificar el archivo `vuetify.js` dentro de la carpeta `plugins`.

* Para agregar textos al archivo de idiomas, favor de modificar el archivo `es.js`que se encuentra dentro de la carpeta `locales`, si desea agregar otro idioma generar un nuevo `js` con la clave de idioma, respetando misma estructura y llaves del archivo `es.js` y con los textos traducidos al idioma correspondiente.


### Autenticación por default
```
email: eve.holt@reqres.in
password: cityslicka
```
Al autenticarse con cualquier usuario siempre recuperara el de Janet Weaver (id: 2), debido a que la API en su endpint de login no devuelve id, por lo que el dato quedo hardcodeado.

### Especificaciones
* Vue 2
* Vuetify
* Axios
* Api: https://reqres.in/
* i18n para internalización
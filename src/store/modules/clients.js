export default {
    namespaced: true,
    state: {
      clients: null
    },
    mutations: {
      setClients: async (state, clients) => {
        if( clients ) {
          state.clients = clients;
        }
      },
      logout: (state)=>{
        state.user = null;
        state.logged = false;
        state.token = null;
      }
    },
    actions: {
      getClients: async ({commit, dispatch}) => {
        let clients = await axios.get('users')
          .then( (response) => {
           commit('setClients', response.data.data)
           return response.data.data;
          })
          .catch( (error) => {
            console.log('Error al recuperar los clientes: ', error)
            return error.response && error.response.data ? error.response.data : error
          })
        return clients;
      },
    },
    getters: {
    }
  }
  
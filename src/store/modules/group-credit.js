export default {
    namespaced: true,
    state: {
      groupDetail: null
    },
    mutations: {
      setGroupDetail: async (state, groupDetail) => {
        if( groupDetail ) {
          state.groupDetail = groupDetail;
        }
      },
    },
    actions: {
      setGroupDetail: async ({commit, dispatch}, groupDetail) => {
          commit('setGroupDetail', groupDetail)
      },
    },
    getters: {
      getGroupDetail: (state) =>{
        return state.groupDetail;
      }
    }
  }
  
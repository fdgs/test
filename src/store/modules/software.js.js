export default {
    namespaced: true,
    state: {
      softwareDetail: null
    },
    mutations: {
      setSoftwareDetail: async (state, softwareDetail) => {
        if( softwareDetail ) {
          state.softwareDetail = softwareDetail;
        }
      },
    },
    actions: {
      setSoftwareDetail: async ({commit, dispatch}, softwareDetail) => {
          commit('setsoftwareDetail', softwareDetail)
      },
    },
    getters: {
      getSoftwareDetail: (state) =>{
        return state.softwareDetail;
      }
    }
  }
  
export default {
  namespaced: true,
  state: {
    user: null,
    logged: false,
    token: null,
    role: 'guest'
  },
  mutations: {
    setUser: async (state, idUser) => {
      if (idUser) {
        await axios.get(`users/${idUser}`)
          .then(response => {
            if (response.data && response.data.data) {
              let userAuth = response.data.data
              state.user = userAuth;
              state.logged = true;
              state.role = 'admin'
              localStorage.setItem('auth', true);
              localStorage.setItem('user', JSON.stringify(userAuth));
            }
          })
          .catch(error => {
            console.log('Ocurrió un error al recuperar el usuario. ')
          })
      }
    },
    logout: (state) => {
      localStorage.removeItem('auth');
      localStorage.removeItem('user');
      state.user = null;
      state.logged = false;
      state.token = null;
    }
  },
  actions: {
    login: async ({ commit, dispatch }, user) => {
      let login = await axios.post('login', user)
        .then((response) => {
          if (response.data.token) {
            dispatch('getUser', 2);
          }
        })
        .catch((error) => {
          return error.response && error.response.data ? error.response.data : error
        })
      return login;
    },
    logout: ({ commit }) => {
      commit('logout');
    },
    getUser: ({ commit }, idUser) => {
      commit('setUser', idUser)
    }
  },
  getters: {
    isLogged: (state) => {
      return !!state.user;
    },
    userLogged: (state) => {
      return state.user
    }
  }
}

import Vue from 'vue'
import Vuex from 'vuex'
import axios from '../plugins/axios'
import VueAxios from 'vue-axios';

import authModule from  '@/store/modules/auth';
import clientsModule from '@/store/modules/clients';
import groupCreditModule from '@/store/modules/group-credit'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default new Vuex.Store({
  state: {
    loading: false,
    alert: {
      type: 'success',
      show: false,
      message: ''
    }
  },
  mutations: {
    setLoading: (state, loading) => {
      state.loading = loading;
    },
    setAlert: (state, data) => {
      state.alert.type = data.type;
      state.alert.show = data.show;
      state.alert.message = data.message;
      setTimeout(() => {
        state.alert.type = 'success';
        state.alert.show = false;
        state.alert.message = '';
      }, data.timeout);
    }
  },
  modules: {
    auth: authModule,
    clients: clientsModule,
    groupCredit: groupCreditModule
  },
  getters: {
    isLoading: (state) =>{
      return state.loading;
    },
  }
})

import axios from 'axios';

const apiAxios = axios.create({
    baseURL:  process.env.VUE_APP_API_URL || 'https://reqres.in/api'
});

export default {
  install: function(Vue) {
    Object.defineProperty(Vue.prototype, '$axios', { value: apiAxios });
  }
}

window.axios = apiAxios;

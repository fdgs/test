import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import colors from 'vuetify/es5/util/colors';
import es from 'vuetify/lib/locale/es';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
      options: {
        customProperties: true,
      },
    themes: {
      light: {
        primary: '#181A24',
        secondary: colors.green.accent3,
        tertiary: '#FFFFFF',
        accent: '#82B1FF',
        error: colors.red.darken2,
        info: colors.blue.darken2,
        success: colors.green.darken2,
        warning: colors.yellow.darken2,
        background: '#080a12',
      },
    },
  },
    lang: {
      locales: { es },
      current: 'es',
    },
});

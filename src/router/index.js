import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import store from '@/store';

const beforeEnter = (to, from, next) => {
  if (store.state.auth.logged) {
    next({path: '/'});
  } else {
    next();
  }
};

const routes = [
  {
    path: '/',
    name: 'Home',
    meta: { Auth: true, title: 'Inicio', breadcrumb: 'Home', showBreadcrumbs: false },
    component: () => import('../views/Home.vue'),
    children: [
      
    ]
  },
  {
    path: '/login',
    name: 'Login',
    meta: { Auth: false, title: 'Iniciar sesión' },
    beforeEnter: (to, from, next) => beforeEnter(to, from, next),
    component: () => import('../views/Login.vue')
  },
  {
    path: '/group-credits',
    name: 'Creditos Grupales',
    meta: { Auth: true, title: 'Creditos Grupales', breadcrumb: 'Búscar créditos grupales', showBreadcrumbs: true, role: 'admin' },
    component: () => import('../views/GroupCredits/GroupCredits.vue'),
    children:[
      {
        path:'/',
        name: 'Creditos Grupales',
        meta: { Auth: true, title: 'Creditos Grupales', breadcrumb: 'Búscar créditos grupales', showBreadcrumbs: true, role: 'admin' },
        component: () => import('../views/GroupCredits/GroupCreditsList.vue'),
      },
      {
        path:'detail',
        name: 'Detalle creditos grupal',
        meta: { Auth: true, title: 'Creditos Grupales', breadcrumb: 'Detalle creditos grupal', showBreadcrumbs: true, role: 'admin' },
        component: () => import('../views/GroupCredits/GroupCreditDetail.vue'),
      },
      {
        path:'inspect',
        name: 'Inspeccionar',
        meta: { Auth: true, title: 'Inspeccionar', breadcrumb: 'Inspeccionar', showBreadcrumbs: true, role: 'admin' },
        component: () => import('../views/GroupCredits/Inspect.vue'),
      
      },
    ]
  },
  {
    path: '/software-list',
    name: 'Listado de Software',
    meta: { Auth: true, title: 'Listado de Software', breadcrumb: 'Listado de Software' , showBreadcrumbs: true,  role: 'admin' },
    component: () => import('../views/software/SoftwareList.vue'),
    children: [
      {
        path:'detail',
        name: 'Detalle software',
        meta: { Auth: true, title: 'Detalle software', breadcrumb: 'Detalle software', showBreadcrumbs: true, role: 'admin' },
        component: () => import('../views/software/SoftwareDetail.vue'),
      },
    ]
  },
  {
    path: '/404',
    name: 'PageNotFound',
    component: () => import('../views/PageNotFound.vue'),
    meta: { Auth: false, title: 'Página no encontrada' },
  },
  { path: '*', redirect: '/404' },  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;

  let auth = localStorage.getItem('auth') || store.state.auth.logged;

  if (to.meta.Auth && !auth) {
    return next({path: '/login'});
  } else {
    /*if (to.meta.role) {
      if (to.meta.role !== store.state.auth.role) {
        return next({path: '/'});
      }
    }*/
    next();
  }
});

export default router

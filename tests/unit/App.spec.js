import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import App from '@/App';
import NavBar from '@/components/NavBar';
import BreadCrumbs from '@/components/utils/BreadCrumbs';

const localVue = createLocalVue();

localVue.use(Vuex)

describe('App', () => {
  let store, state, getters, actions, mutations, $t

  beforeEach(() => {
    $t = () => { }

    getters = {
      'auth/isLogged': () => false,
      isLoading: () => false
    }

    actions = {
      'auth/getUser': jest.fn()
    }

    mutations = {
      setLoading: jest.fn()
    }

    state = {
      loading: () => false
    }

    store = new Vuex.Store({
      actions,
      mutations,
      getters,
      state
    })
  })

  const build = () => {
    const wrapper = shallowMount(App, {
      store,
      localVue,
      mocks: { $t }
    })

    return {
      wrapper,
      NavBar: () => wrapper.findComponent(NavBar),
      BreadCrumbs: () => wrapper.findComponent(BreadCrumbs),
    }
  }

  it("has data", () => {
    expect(typeof App.data).toBe("function");
  });

  it('Render App', () => {
    const { wrapper } = build();

    expect(wrapper.html()).toMatchSnapshot()
  })

  it('Validate sesion', () => {
    jest.useFakeTimers();
    const mockSetItem = jest.fn(localStorage.setItem);
    localStorage.setItem = mockSetItem;
    localStorage.setItem('auth', true);

    localStorage.setItem('user', JSON.stringify({ id: 1 }));

    const { wrapper } = build();

    expect(wrapper.html()).toMatchSnapshot()
    jest.advanceTimersByTime(1000);
  })
})
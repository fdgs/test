import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import NavBar from '@/components/NavBar';

const localVue = createLocalVue();

localVue.use(Vuex)

describe('NavBar', () => {
    let store, getters, actions, $t

    const mockRouter = {
        push: jest.fn()
    }

    beforeEach(() => {
        $t = () => { }

        getters = {
            'auth/userLogged': () => false,
            'auth/isLogged': () => false
        }

        actions = {
            'auth/logout': jest.fn()
        }

        store = new Vuex.Store({
            getters,
            actions
        })
    })

    const build = () => {
        const wrapper = shallowMount(NavBar, {
            store,
            localVue,
            mocks: { $t, $router: mockRouter }
        })

        return {
            wrapper
        }
    }

    it("has data", () => {
        expect(typeof NavBar.data).toBe("function");
    });

    it('Render NavBar', () => {
        const { wrapper } = build()

        expect(wrapper.html()).toMatchSnapshot()
    })

    it('correctly onClickLogout is called', () => {
        const { wrapper } = build()

        wrapper.vm.onClickMenu('Home');

        expect(mockRouter.push).toHaveBeenCalledTimes(1);

        jest.resetAllMocks();
    })

    it('correctly onClickLogout is called', () => {
        const { wrapper } = build()

        wrapper.vm.onClickLogout();

        expect(mockRouter.push).toHaveBeenCalledTimes(1);
        jest.resetAllMocks();
    })

    it('correctly toHome is called', () => {
        const { wrapper } = build()

        wrapper.vm.toHome();

        expect(mockRouter.push).toHaveBeenCalledTimes(1);
        jest.resetAllMocks();
    })
})

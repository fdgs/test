import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import SnackBar from '@/components/utils/SnackBar';

const localVue = createLocalVue();

localVue.use(Vuex)

describe('SnackBar', () => {
    let store, getters, actions, $t

    const mockRouter = {
        push: jest.fn()
    }

    beforeEach(() => {
        $t = () => { }

        getters = {
            'auth/userLogged': () => false,
            'auth/isLogged': () => false
        }

        actions = {
            'auth/logout': jest.fn()
        }

        store = new Vuex.Store({
            getters,
            actions
        })
    })

    const build = () => {
        const wrapper = shallowMount(SnackBar, {
            store,
            localVue,
            mocks: { $t, $router: mockRouter }
        })

        return {
            wrapper
        }
    }

    it("has data", () => {
        expect(typeof SnackBar.data).toBe("function");
    });

    it('Render SnackBar', () => {
        const { wrapper } = build()

        expect(wrapper.html()).toMatchSnapshot()
    })
})

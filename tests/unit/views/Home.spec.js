import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Home from '@/views/Home';

const localVue = createLocalVue();

localVue.use(Vuex)

let user = {
    first_name: 'Juan',
    last_name: 'Pérez',
    email: 'email@email.com'
}

describe('Home', () => {
    let store, getters, actions, $t

    beforeEach(() => {
        $t = () => { }

        getters = {
            'auth/userLogged': () => false,
            user: () => user
        }

        store = new Vuex.Store({
            getters
        })
    })

    const build = () => {
        const wrapper = shallowMount(Home, {
            store,
            localVue,
            mocks: { $t }
        })

        return {
            wrapper
        }
    }

    it('Render Home', () => {
        const { wrapper } = build()

        expect(wrapper.html()).toMatchSnapshot()
    })
})



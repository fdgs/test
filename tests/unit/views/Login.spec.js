import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Login from '@/views/Login';
import AppSnackBar from '@/components/utils/SnackBar';
import Vue from 'vue';

const localVue = createLocalVue();

localVue.use(Vuex)

describe('Login', () => {
    let store, getters, actions, mutations, state, $t

    const mockRouter = {
        push: jest.fn()
    }

    beforeEach(() => {
        $t = () => { }

        getters = {
            'auth/isLogged': () => false,
            isLoading: () => false
        }

        actions = {
            'auth/login': jest.fn()
        }

        state = {

        }

        mutations = {
            setLoading: jest.fn()
        }

        store = new Vuex.Store({
            actions,
            mutations,
            getters,
            state
        })
    })

    afterEach(() => {
        jest.resetAllMocks();
    })

    const build = () => {

        const wrapper = shallowMount(Login, {
            store,
            localVue,
            mocks: { $t,  $router: mockRouter }
        })

        return {
            wrapper,
            email: () => wrapper.findAllComponents({ name: 'v-text-field' }).at(0),
            password: () => wrapper.findAllComponents({ name: 'v-text-field' }).at(1),
            button: () => wrapper.findComponent({ name: 'v-btn' })
        }
    }

    it("has data", () => {
        expect(typeof Login.data).toBe("function");
    });

    it('Render Login', () => {
        const { wrapper } = build()

        expect(wrapper.html()).toMatchSnapshot()
    })

    it('renders components', () => {
        const { email, password, button } = build()

        expect(email().exists()).toBe(true)
        expect(password().exists()).toBe(true)
        expect(button().exists()).toBe(true)
    })

    it('form invalid', async () => {
        const { wrapper, email, password, button } = build() 
        let expectEmail = ''
        let expectPassword = ''

        email().element.value = expectEmail
        password().element.value  = expectPassword

        email().trigger('input')
        password().trigger('input')
        await Vue.nextTick()

        expect(email().element.value).toBe(expectEmail)
        expect(password().element.value).toBe(expectPassword)
        expect(wrapper.vm.valid).toBe(false)
    })

    it('form valid', async () => {
        const { wrapper, email, password, button } = build()
        let expectEmail = 'test@email.com'
        let expectPassword = '1234567890'

        email().element.value = expectEmail
        password().element.value  = expectPassword

        email().trigger('input')
        password().trigger('input')
        await Vue.nextTick()

        expect(email().element.value).toBe(expectEmail)
        expect(password().element.value).toBe(expectPassword)
    })

    it('Catch login without error.error', () =>{
        jest.useFakeTimers();
        const { wrapper } = build()
        wrapper.vm.timeout = 500
        const mockResponse = new Error('Error')

        const spyLogin = jest.spyOn(actions, 'auth/login').mockRejectedValue(mockResponse)
        wrapper.vm.submit();

        jest.advanceTimersByTime(1000);
        expect(spyLogin).toHaveBeenCalledTimes(1)
    })

    it('Catch login with error.error', () =>{
        jest.useFakeTimers();
        const { wrapper } = build()
        wrapper.vm.timeout = 500
        const mockResponse = { error: { error: 'ocurrió un error' } }

        const spyLogin = jest.spyOn(actions, 'auth/login').mockRejectedValue(mockResponse)
        wrapper.vm.submit();

        jest.advanceTimersByTime(1000);
        expect(spyLogin).toHaveBeenCalledTimes(1)
    })

    it('Error login is called', () => {
        jest.useFakeTimers();
        const { wrapper } = build()
        wrapper.vm.timeout = 500

        const mockResponse = { error: 'ocurrió un error' } 

        const spyLogin = jest.spyOn(actions, 'auth/login').mockReturnValue(mockResponse)

        wrapper.vm.submit();

        expect(spyLogin).toHaveBeenCalledTimes(1)

        jest.advanceTimersByTime(1000);
    })

    it('OK login is called', () => {
        jest.useFakeTimers();
        const { wrapper } = build()

        const spyLogin = jest.spyOn(actions, 'auth/login').mockResolvedValue(null)

        wrapper.vm.submit();

        jest.advanceTimersByTime(1000);
        expect(spyLogin).toHaveBeenCalledTimes(1)
    })
})
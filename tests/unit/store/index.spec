import index from '@/store/index'

describe('Index Storage', () => {
    let state;

    beforeEach(() => {
        state = {
            loading: false,
            alert: {
                type: 'success',
                show: false,
                message: ''
            }
        }
    })

    it('setLoading', () => {
        const expectedLoading = true;

        index.mutations.setLoading(true)

        expect(state.loading).toBe(true);
    })
})